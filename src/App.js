import React, { useState } from "react";
import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";

const DUMMY_EXPENSES = [
  { 
    id: 1, 
    title: "Buy new Car",
    amount: 50000, 
    date: new Date(2021, 11, 2) 
  },
  {
    id: 2,
    title: "Buy new House",
    amount: 150000,
    date: new Date(2022, 11, 3),
  },
  {
    id: 3,
    title: "Buy new Mac",
    amount: 250000,
    date: new Date(2021, 11, 3),
  },
];
const App = () => {
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  const addExpenseData = (expense) => {
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  };

  return (
    <div>
      <NewExpense onAddExpense={addExpenseData} />
      <Expenses items={expenses} />
    </div>
  );

  // This is how JSX works with React:
  // return React.createElement(
  //   "div",
  //   {},
  //   React.createElement("h2", {}, "hoaphamm"),
  //   React.createElement(Expenses, {items:expenses})
  // );
};

export default App;
