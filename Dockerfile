FROM node:14.18
WORKDIR /app
COPY . .
RUN npm install
CMD ["npm","start"]